from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.views import View
from django.views.generic import TemplateView
from django.views.generic import DetailView
# from django.template import loader
from .models import Question, Choice


class IndexView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        latest_question_list = Question.objects.order_by('-pub_date')[:5]
        context['latest_question_list'] = latest_question_list
        return context


"""
class IndexView(View):
    def get(self, request):
        latest_question_list = Question.objects.order_by('-pub_date')[:5]
        template = 'index.html'
        context = {
            'latest_question_list': latest_question_list
        }

        return render(request, template, context)
"""


# def index(request):
# latest_question_list = Question.objects.order_by('-pub_date')[:5]
# template = loader.get_template('polls/index.html')
# return HttpResponse(template.render(context, request))

class DetailsView(View):
    def get(self, request, question_id):
        question = get_object_or_404(Question, pk=question_id)
        return render(request, 'polls/detail.html', {'question': question})

        # try:
        # except Question.DoesNotExist:
        # raise Http404("Question does not exist")
    # return HttpResponse("You're looking at question %s." % question_id)


class ResultsView(View):
    def get(self, request, question_id):
        question = get_object_or_404(Question, pk=question_id)
        return render(request, 'polls/results.html', {'question': question})


# def results(request, question_id):
#    question = get_object_or_404(Question, pk=question_id)
#    return render(request, 'polls/results.html', {'question': question})
#   # response = "You're looking at the results of question %s."
#  # return HttpResponse(response % question_id)

class VoteView(View):
    def post(self, request, question_id):
        question = get_object_or_404(Question, pk=question_id)
        try:
            selected_choice = question.choice_set.get(pk=request.POST['choice'])
        except (KeyError, Choice.DoesNotExist):
                # Redisplay the question voting form.
            return render(request, 'polls/detail.html', {
                'question': question,
                'error_message': "Não selecionou nenhuma questão.",
            })
        else:
            selected_choice.votes += 1
            selected_choice.save()
                # Always return an HttpResponseRedirect after successfully dealing
                # with POST data. This prevents data from being posted twice if a
                # user hits the Back button.
            return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))
