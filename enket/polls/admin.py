from django.contrib import admin

# Register your models here.
from .models import Question, Choice

# admin.site.register(Question)


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    list_display = ("question_text", "pub_date")
    list_display_links = ("question_text",)
    search_fields = ["question_text", "pub_date"]
    list_filter = ("question_text",)
    # list_per_page = 1


@admin.register(Choice)
class ChoiceAdmin(admin.ModelAdmin):
    list_display = ("question", "choice_text",)


