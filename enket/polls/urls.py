from django.urls import path
from polls.views import IndexView, DetailsView, ResultsView, VoteView
from . import views
from django.views.generic import TemplateView

app_name = 'polls'

urlpatterns = [
    # ex: /polls/
    path('', IndexView.as_view()),
    # path('', views.index, name='index'),
    # ex: /polls/5/
    path('<int:question_id>/', DetailsView.as_view(), name='detail'),
    # path('<int:question_id>/', views.detail, name='detail'),
    # ex: /polls/5/results/
    path('<int:question_id>/results/', ResultsView.as_view(), name='results'),
    # path('<int:question_id>/results/', views.results, name='results'),
    # ex: /polls/5/vote/
    path('<int:question_id>/vote/', VoteView.as_view(), name='vote'),
    # path('<int:question_id>/vote/', views.vote, name='vote'),
]